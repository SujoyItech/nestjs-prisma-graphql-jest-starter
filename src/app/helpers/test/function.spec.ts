import { BadRequestException, INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { LocalizationModule } from '@squareboat/nestjs-localization/dist/src';
import { join } from 'path';
import { PrismaService } from '../../../libs/prisma/prisma.service';
import { ResponseModel } from '../../../app/models/dto/response.model';
import {
  convertToSlug,
  errorResponse,
  getRandomNumber,
  IgnoreUnique,
  prisma_client,
  processException,
  randomString,
  setApp,
  successResponse,
  uniqueCode,
} from '../functions';
import { STATUS_CODE_200 } from '../coreconstants';

describe('Function', () => {
  let app: INestApplication;
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        LocalizationModule.register({
          path: join(process.env.BASE_PROJECT_PATH, 'resources/lang/'),
          fallbackLang: 'en',
        }),
      ],
      providers: [PrismaService],
    }).compile();
    app = module.createNestApplication();
    setApp(app);
    await app.init();
  });

  describe('getRandomNumber', () => {
    it('Should return 6 digit random number', () => {
      const randomNumber = getRandomNumber(6);
      expect(randomNumber).toHaveLength(6);
    });
  });

  describe('randomString', () => {
    it('Should return 6 digit random string', () => {
      const randomStringValue = randomString(6);
      expect(randomStringValue).toHaveLength(6);
    });
  });

  describe('convertToSlug', () => {
    it('Should return slug of any text', () => {
      const value = convertToSlug('Hello $- My # name IS SUJoy');
      expect(value).toBe('hello-my-name-is-sujoy');
    });
  });

  describe('uniqueCode', () => {
    it('Should return uniqueCode', () => {
      const value = uniqueCode('Sujoy', 'Upo');
      expect(value).toContain('Sujoy');
    });
  });

  describe('successResponse', () => {
    it('Should return successResponse', () => {
      const message = 'Test success response';
      const data = {
        name: 'Test',
        roll: 1,
      };
      const code = 200;
      const value = successResponse(message, data, code);
      expect(value).toEqual({
        success: true,
        message: message,
        data: data,
        code: code,
      });
    });
  });

  describe('errorResponse', () => {
    it('Should return errorResponse', () => {
      const message = 'Test error response';
      const data = {
        name: 'Test',
        roll: 1,
      };
      const code = 500;
      const value = errorResponse(message, data, code);
      expect(value).toEqual({
        success: false,
        message: message,
        messages: [],
        data: data,
        code: code,
      });
    });
  });

  describe('processException', () => {
    it('Should return processException', () => {
      try {
        throw new BadRequestException('Testing error');
      } catch (e) {
        try {
          processException(e);
        } catch (e2) {
          expect(e2).toBeInstanceOf(Error);
          expect(e2.message).toBe('Testing error');
        }
      }
    });
  });

  describe('IgnoreUnique', () => {
    it('Check unique in any prisma model', async () => {
      const category = await prisma_client.category.create({
        data: {
          title: 'Ignore test title',
        },
      });
      const response = await IgnoreUnique(
        'Ignore test title',
        'Category',
        'title',
        category.id,
      );
      expect(response.success).toBeTruthy();
      expect(response.code).toBe(STATUS_CODE_200);
      await prisma_client.category.delete({
        where: {
          id: category.id,
        },
      });
    });
  });
});
