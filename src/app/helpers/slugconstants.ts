//settings value type
export const SETTINGS_VALUE_TYPE_TEXT = 1;
export const SETTINGS_VALUE_TYPE_MEDIA_URL = 2;
//

//admin settings slugs
export const SETTINGS_GROUP_APPLICATION = 'application_settings';
export const SETTINGS_GROUP_GENERAL = 'general_settings';
export const SETTINGS_GROUP_HOMEPAGE = 'homepage_settings';
export const SETTINGS_GROUP_FOOTER = 'footer_settings';
export const SETTINGS_GROUP_EMAIL = 'email_settings';
export const SETTINGS_GROUP_LOGO = 'logo_settings';
export const SETTINGS_GROUP_BLOCKCHAIN = 'blockchain_settings';
export const SETTINGS_GROUP_SOCIAL = 'social_settings';
//

// Geneal settings
export const SETTINGS_SLUG_APPLICATION_TITLE = 'application_title';
export const SETTINGS_SLUG_CONTRACT_EMAIL = 'contract_email';
export const SETTINGS_SLUG_CONTRACT_PHONE = 'contract_phone';
export const SETTINGS_SLUG_ADDRESS = 'address';
export const SETTINGS_SLUG_COPY_RIGHT = 'copy_right_text';
export const SETTINGS_SLUG_WALLET_ADDRESS = 'wallet_address';
export const SETTINGS_SLUG_ADMIN_COMMISSION = 'admin_commission';
//

// Email settings
export const SETTINGS_SLUG_MAIL_DIRIVER = 'mail_driver';
export const SETTINGS_SLUG_MAIL_HOST = 'mail_host';
export const SETTINGS_SLUG_MAIL_PORT = 'mail_port';
export const SETTINGS_SLUG_MAIL_USERNAME = 'mail_username';
export const SETTINGS_SLUG_MAIL_PASSWORD = 'mail_password';
export const SETTINGS_SLUG_MAIL_ENCRYPTION = 'mail_encryption';
export const SETTINGS_SLUG_MAIL_FROM_ADDRESS = 'mail_from_address';
export const SETTINGS_SLUG_MAIL_FROM_NAME = 'mail_from_name';
//

// Logo settings
export const SETTINGS_SLUG_APP_LOGO_LARGE = 'app_logo_large';
export const SETTINGS_SLUG_APP_LOGO_SMALL = 'app_logo_small';
export const SETTINGS_SLUG_FAVICON_LOGO = 'favicon_logo';

//

// Social settings
export const SETTINGS_SLUG_FACEBOOK_LINK = 'facebook_link';
export const SETTINGS_SLUG_TWITTER_LINK = 'twitter_link';
export const SETTINGS_SLUG_INSTAGRAM_LINK = 'instagram_link';
export const SETTINGS_SLUG_DISCORD_LINK = 'discord_link';
export const SETTINGS_SLUG_WHATSAPP_LINK = 'whatsapp_link';
export const SETTINGS_SLUG_LINKEDIN_LINK = 'linkedin_link';
