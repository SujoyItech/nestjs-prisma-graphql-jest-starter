import { Field, ObjectType } from '@nestjs/graphql';
import { imageLinkAddMiddleware } from '../middlewares/imageLinkAdd.middleware';

@ObjectType()
export class Category {
  @Field(() => Number)
  id: number;
  @Field()
  title: string;
  @Field({
    nullable: true,
    middleware: [imageLinkAddMiddleware],
  })
  image: string;
  @Field(() => Number)
  status: number;
}
