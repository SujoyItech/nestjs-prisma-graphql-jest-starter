import { Field, InputType, ObjectType } from '@nestjs/graphql';
@InputType()
export class StaffOrder {
  @Field(() => String)
  field: string;
  @Field(() => String)
  direction: string;
}
