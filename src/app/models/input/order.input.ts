import { Field, InputType, ObjectType } from '@nestjs/graphql';

@InputType()
export class Order {
  @Field(() => String)
  field: string;
  @Field(() => String)
  direction: string;
}
