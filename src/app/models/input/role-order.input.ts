import { Field, InputType } from '@nestjs/graphql';
@InputType()
export class RoleOrder {
  @Field(() => String)
  field: string;
  @Field(() => String)
  direction: string;
}
