import { Module } from '@nestjs/common';
import { PasswordService } from '../../../libs/auth/password.service';
import { StaffResolver } from './staff.resolver';
import { StaffService } from './staff.service';
import { StaffAuthModule } from './auth/staff-auth.module';
import { CategoryModule } from './category/category.module';
import { RoleModule } from './role/role.module';

@Module({
  imports: [StaffAuthModule, CategoryModule, RoleModule],
  providers: [StaffResolver, StaffService, PasswordService],
})
export class StaffModule {}
