import { Field, Int, ObjectType } from '@nestjs/graphql';
import { settingsMediaLink } from '../../../middlewares/settingsMediaLinkAdd.middleware';

@ObjectType()
export class Setting {
  @Field(() => Int)
  id: number;

  @Field(() => Int, { nullable: true })
  value_type: number;

  @Field(() => String, { nullable: true })
  option_group: string;

  @Field(() => String)
  option_key: string;

  @Field(() => String, {
    nullable: true,
    middleware: [settingsMediaLink],
  })
  option_value: string;
}
