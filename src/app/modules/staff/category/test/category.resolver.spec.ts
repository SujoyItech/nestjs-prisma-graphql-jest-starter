import { ConfigService } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { CategoryResolver } from '../category.resolver';
import { CategoryService } from '../category.service';

describe('CategoryResolver', () => {
  let categoryResolver: CategoryResolver;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [],
      providers: [ConfigService, CategoryResolver, CategoryService],
    })
      .overrideProvider(CategoryService)
      .useValue({})
      .compile();
    categoryResolver = module.get<CategoryResolver>(CategoryResolver);
  });

  it('Should be defined', () => {
    expect(categoryResolver).toBeDefined();
  });
});
