import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { prisma_client, setApp } from '../../../../../app/helpers/functions';
import { PrismaService } from '../../../../../../src/libs/prisma/prisma.service';
import { FilesystemService } from '../../../../filesystem/filesystem.service';
import {
  STATUS_ACTIVE,
  STATUS_CODE_200,
} from '../../../../helpers/coreconstants';

import { CategoryService } from '../category.service';
import { CreateCategoryDto, UpdateCategoryDto } from '../dto/create.dto';
import { LocalizationModule } from '@squareboat/nestjs-localization/dist/src';
import { join } from 'path';

const createCategoryInput: CreateCategoryDto = {
  title: 'Test Create Category',
  image: null,
  imageFile: null,
  status: STATUS_ACTIVE,
};

const updateCategoryInput: UpdateCategoryDto = {
  title: 'Test Update Category',
  image: null,
  imageFile: null,
  status: STATUS_ACTIVE,
};

describe('CategoryService', () => {
  //twst
  let categoryService: CategoryService;
  let app: INestApplication;
  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        LocalizationModule.register({
          path: join(process.env.BASE_PROJECT_PATH, 'resources/lang/'),
          fallbackLang: 'en',
        }),
      ],
      providers: [
        CategoryService,
        PrismaService,
        {
          provide: FilesystemService,
          useValue: {},
        },
      ],
    }).compile();

    app = module.createNestApplication();
    setApp(app);
    await app.init();

    categoryService = module.get<CategoryService>(CategoryService);
  });

  it('Should be defined', () => {
    expect(categoryService).toBeDefined();
  });

  describe('createCategory', () => {
    it('Should returns an category', async () => {
      const result = await categoryService.createCategory(createCategoryInput);
      expect(result.code).toBe(STATUS_CODE_200);
      expect(result.success).toBeTruthy();

      const category = await prisma_client.category.findFirst({
        where: { title: createCategoryInput.title },
      });
      expect(category.title).toBe(createCategoryInput.title);
    });

    it('Should return error', async () => {
      try {
        await categoryService.createCategory(createCategoryInput);
      } catch (e) {
        expect(e).toBeInstanceOf(Error);
      }
    });
  });

  describe('updateCategory', () => {
    it('Should returns an category', async () => {
      const catgegory = await prisma_client.category.create({
        data: {
          title: 'Update Category test',
        },
      });
      const result = await categoryService.updateCategory(
        catgegory.id,
        updateCategoryInput,
      );
      expect(result.code).toBe(STATUS_CODE_200);
      expect(result.success).toBeTruthy();
    });
  });

  describe('getCategories', () => {
    it('Should returns the arrays of categories', async () => {
      await prisma_client.category.createMany({
        data: [
          { title: 'Test 1' },
          { title: 'Test 2' },
          { title: 'Test 3' },
          { title: 'Test 4' },
        ],
      });
      const result = await categoryService.getCategories();
      expect(result).toBeInstanceOf(Array);
    });
  });

  describe('getCategoryById', () => {
    it('Should returns the category', async () => {
      const getCat = await prisma_client.category.create({
        data: {
          title: 'Get category',
        },
      });
      const result = await categoryService.getCategoryById(getCat.id);
      expect(result.id).toBe(getCat.id);
    });
  });

  describe('deleteCategory', () => {
    it('Should returns an category', async () => {
      const deleteCate = await prisma_client.category.create({
        data: {
          title: 'Delete category',
        },
      });
      const result = await categoryService.deleteCategory(deleteCate.id);
      expect(result.code).toBe(STATUS_CODE_200);
      expect(result.success).toBeTruthy();
    });
  });

  afterEach(async () => {
    await prisma_client.category.deleteMany({});
  });
});
