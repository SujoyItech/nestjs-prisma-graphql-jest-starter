import { Module } from '@nestjs/common';
import { CategoryService } from './category.service';
import { CategoryResolver } from './category.resolver';
import { FilesystemService } from '../../../filesystem/filesystem.service';
import { ConfigModule, ConfigService } from '@nestjs/config';

@Module({
  providers: [CategoryService, CategoryResolver],
})
export class CategoryModule {}
