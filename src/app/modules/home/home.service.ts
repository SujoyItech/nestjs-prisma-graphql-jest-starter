/* eslint-disable @typescript-eslint/no-var-requires */
import { BadRequestException, Injectable } from '@nestjs/common';
import { isEmail } from 'class-validator';
import {
  GLOBAL_SEARCH_QUERY_LIMIT,
  STATUS_ACTIVE,
} from '../../../app/helpers/coreconstants';
import {
  errorResponse,
  prisma_client,
  processException,
  successResponse,
  __,
} from '../../helpers/functions';

@Injectable()
export class HomeService {}
