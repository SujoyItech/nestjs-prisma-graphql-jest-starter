/* eslint-disable @typescript-eslint/no-var-requires */
import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { HomeService } from './home.service';

@Resolver()
export class HomeResolver {
  constructor(private readonly homeService: HomeService) {}
}
