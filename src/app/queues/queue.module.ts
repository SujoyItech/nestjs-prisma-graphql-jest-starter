import { Global, Module } from '@nestjs/common';
import { BullModule } from '@nestjs/bull';
import { UserModule } from '../modules/user/user.module';

@Global()
@Module({
  imports: [UserModule],
  providers: [],
  exports: [],
})
export class QueueModule {}
