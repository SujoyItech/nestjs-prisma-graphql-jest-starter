/* eslint-disable prettier/prettier */
/* eslint-disable @typescript-eslint/no-var-requires */
const Moralis = require('moralis/node');
const Web3 = require('web3');

/* const serverUrl = 'https://aqnwizphnaol.usemoralis.com:2053/server';
const appId = 'cLarzWdCgjQj2JtucOVCPTK8rY4m31qIh11SywzR';
const masterKey = 'yhZenxZoiWYYzx8cNiMAvIZ3dvUC7DqYo8jsbr7b'; */

const ERC721_ABI = `[{"inputs":[],"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"owner","type":"address"},{"indexed":true,"internalType":"address","name":"approved","type":"address"},{"indexed":true,"internalType":"uint256","name":"tokenId","type":"uint256"}],"name":"Approval","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"owner","type":"address"},{"indexed":true,"internalType":"address","name":"operator","type":"address"},{"indexed":false,"internalType":"bool","name":"approved","type":"bool"}],"name":"ApprovalForAll","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"from","type":"address"},{"indexed":true,"internalType":"address","name":"to","type":"address"},{"indexed":true,"internalType":"uint256","name":"tokenId","type":"uint256"}],"name":"Transfer","type":"event"},{"inputs":[{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"tokenId","type":"uint256"}],"name":"approve","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"owner","type":"address"}],"name":"balanceOf","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"baseTokenURI","outputs":[{"internalType":"string","name":"","type":"string"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"tokenId","type":"uint256"}],"name":"getApproved","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"owner","type":"address"},{"internalType":"address","name":"operator","type":"address"}],"name":"isApprovedForAll","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"recipient","type":"address"},{"internalType":"string","name":"uri","type":"string"}],"name":"mint","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"name","outputs":[{"internalType":"string","name":"","type":"string"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"tokenId","type":"uint256"}],"name":"ownerOf","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"from","type":"address"},{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"tokenId","type":"uint256"}],"name":"safeTransferFrom","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"from","type":"address"},{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"tokenId","type":"uint256"},{"internalType":"bytes","name":"_data","type":"bytes"}],"name":"safeTransferFrom","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"operator","type":"address"},{"internalType":"bool","name":"approved","type":"bool"}],"name":"setApprovalForAll","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"bytes4","name":"interfaceId","type":"bytes4"}],"name":"supportsInterface","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"symbol","outputs":[{"internalType":"string","name":"","type":"string"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"tokenId","type":"uint256"}],"name":"tokenURI","outputs":[{"internalType":"string","name":"","type":"string"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"from","type":"address"},{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"tokenId","type":"uint256"}],"name":"transferFrom","outputs":[],"stateMutability":"nonpayable","type":"function"}]`;
const nftAddress = '0x015EEE8C0Ff2f7f9738Fc579347658bbb7D8Dd6F';
//rinkey '0x015EEE8C0Ff2f7f9738Fc579347658bbb7D8Dd6F';
//ropsten '0x0228A456B4719Dd584230202b9FF47c986Ad7893';

const privateKey = '673d6685548a71cfcc282b5d44f7f0e3256be1580cc915aea2ffe8b00b5bc3cf';
let nftContract;
let web3;
// console.log(nftContract);

async function startMoralis(web3Node = '') {
  if(web3Node) web3 = new Web3(web3Node);
  else {
    await Moralis.start({
      moralisSecret:
        'uKhRFNHBI8E8pkfvSghf4CeMlhqMonn0X38LPqXS6FsVUTn37uuwPlWwktQotyk7',
    });
    await Moralis.enableWeb3({
      chainId: '0x4',
      /* privateKey:
        '673d6685548a71cfcc282b5d44f7f0e3256be1580cc915aea2ffe8b00b5bc3cf', */
    });
    web3 = new Web3(Moralis.provider);
  } 
  const account = web3.eth.accounts.privateKeyToAccount(privateKey);
  web3.eth.defaultAccount = account.address;
  nftContract = new web3.eth.Contract(JSON.parse(ERC721_ABI), nftAddress);
}

async function getNfts() {
  await startMoralis();
  const options = {
    chain: Moralis.Chains.ETH_RINKBEY, //0x4
    address: '0x9908CbCb070d1ed8d8f2c064b281D3029545b185',
    token_address: '0x015EEE8C0Ff2f7f9738Fc579347658bbb7D8Dd6F',
  };
  const nft = await Moralis.Web3API.account.getNFTsForContract(options);
  console.log(nft);
}
// getNfts();

async function getBalance() {
  await startMoralis();
  const balance = await web3.eth.getBalance(
    '0x9908CbCb070d1ed8d8f2c064b281D3029545b185',
  );
  console.log(web3.utils.fromWei(balance));
}
// getBalance();

async function setApprovalForAll() {
  await startMoralis('https://speedy-nodes-nyc.moralis.io/66e2c85a937e07662531585b/eth/rinkeby');
  const call = nftContract.methods.setApprovalForAll(
    '0xe7036EeD3Ea5961156032AA8920fa088B660eC1D',
    true,
  );
  const estimatedGas = await call.estimateGas();
  console.log(estimatedGas);
  const tx = {
    to: nftAddress,
    gas: 2000000,
    data: call.encodeABI(),
  };
  const signedTx = await web3.eth.accounts.signTransaction(tx, privateKey);
  const receipt = await web3.eth.sendSignedTransaction(signedTx.rawTransaction);
  // await getConfirmedTransaction(receipt);
  // console.log(receipt);
}
// setApprovalForAll();

async function getConfirmedTransaction(txObj) {
  let confirmations = 0;
  while (confirmations < 4) {
    const currentBlock = await web3.eth.getBlockNumber();
    confirmations = currentBlock - txObj.blockNumber;
  }
  const tx = await web3.eth.getTransaction(txObj.transactionHash);
  if (!tx) throw new Error(`Transaction Failed: ${txObj.transactionHash}`);
  return;
}

// 0x2c2a29e6ac84b4fb0015723a10cf67b9d2314e5935b0e7182ea052f182e1ce18  f
// 0xf37f9b997573cef96add4dffc5661ec25838b9943e95c9ae1d7178a71be25a9d  s
async function getTransaction(txHash = '0x11fb7280f0d74f8df4203e53b284814604db9e91c091742fbeaa11b418d867bc') {
  await startMoralis();
  const tx = await web3.eth.getTransactionReceipt(txHash);
  // console.log(tx);
  console.log(web3.utils.hexToString(tx.logs[0].data));
  console.log(web3.utils.toUtf8(tx.logs[0].data));
  console.log(web3.utils.toAscii(tx.logs[0].data));
}
// getTransaction();

async function Event() {
  await startMoralis('wss://speedy-nodes-nyc.moralis.io/66e2c85a937e07662531585b/eth/rinkeby/ws');
  nftContract.events.ApprovalForAll({
    //filter: {myIndexedParam: [20,23], myOtherIndexedParam: '0x123456789...'}, // Using an array means OR: e.g. 20 or 23
    //fromBlock: 0
  } /*, function(error, event){ console.log(event); } */)
    .on("connected", function (subscriptionId) {
      console.log(subscriptionId);
    })
    .on('data', function (event) {
      console.log('on data');
      console.log(event); // same results as the optional callback above
    })
    .on('changed', function (event) {
      console.log('on changed');
      console.log(event);
      // remove event from local database

    })
    .on('error', function (error, receipt) { // If the transaction was rejected by the network with a receipt, the second parameter will be the receipt.
      console.log('on error');
      console.log(error);
      console.log(receipt);
    });
}
// Event();

async function test() {
  await startMoralis();
  console.log(web3.currentProvider);
  const count = await web3.eth.getTransactionCount('0x31d4Ae75Cd68f3eaEfAa8b77fF1bc505FBC32184');
  console.log(count);
}
test();