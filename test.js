/* eslint-disable prettier/prettier */
/* eslint-disable @typescript-eslint/no-var-requires */

// const ERC721_ABI = [{"inputs":[],"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"owner","type":"address"},{"indexed":true,"internalType":"address","name":"approved","type":"address"},{"indexed":true,"internalType":"uint256","name":"tokenId","type":"uint256"}],"name":"Approval","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"owner","type":"address"},{"indexed":true,"internalType":"address","name":"operator","type":"address"},{"indexed":false,"internalType":"bool","name":"approved","type":"bool"}],"name":"ApprovalForAll","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"from","type":"address"},{"indexed":true,"internalType":"address","name":"to","type":"address"},{"indexed":true,"internalType":"uint256","name":"tokenId","type":"uint256"}],"name":"Transfer","type":"event"},{"inputs":[{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"tokenId","type":"uint256"}],"name":"approve","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"owner","type":"address"}],"name":"balanceOf","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"baseTokenURI","outputs":[{"internalType":"string","name":"","type":"string"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"tokenId","type":"uint256"}],"name":"getApproved","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"owner","type":"address"},{"internalType":"address","name":"operator","type":"address"}],"name":"isApprovedForAll","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"recipient","type":"address"},{"internalType":"string","name":"uri","type":"string"}],"name":"mint","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"name","outputs":[{"internalType":"string","name":"","type":"string"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"tokenId","type":"uint256"}],"name":"ownerOf","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"from","type":"address"},{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"tokenId","type":"uint256"}],"name":"safeTransferFrom","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"from","type":"address"},{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"tokenId","type":"uint256"},{"internalType":"bytes","name":"_data","type":"bytes"}],"name":"safeTransferFrom","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"operator","type":"address"},{"internalType":"bool","name":"approved","type":"bool"}],"name":"setApprovalForAll","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"bytes4","name":"interfaceId","type":"bytes4"}],"name":"supportsInterface","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"symbol","outputs":[{"internalType":"string","name":"","type":"string"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"tokenId","type":"uint256"}],"name":"tokenURI","outputs":[{"internalType":"string","name":"","type":"string"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"from","type":"address"},{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"tokenId","type":"uint256"}],"name":"transferFrom","outputs":[],"stateMutability":"nonpayable","type":"function"}];
const ERC721_ABI = `[{"inputs":[],"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"owner","type":"address"},{"indexed":true,"internalType":"address","name":"approved","type":"address"},{"indexed":true,"internalType":"uint256","name":"tokenId","type":"uint256"}],"name":"Approval","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"owner","type":"address"},{"indexed":true,"internalType":"address","name":"operator","type":"address"},{"indexed":false,"internalType":"bool","name":"approved","type":"bool"}],"name":"ApprovalForAll","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"from","type":"address"},{"indexed":true,"internalType":"address","name":"to","type":"address"},{"indexed":true,"internalType":"uint256","name":"tokenId","type":"uint256"}],"name":"Transfer","type":"event"},{"inputs":[{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"tokenId","type":"uint256"}],"name":"approve","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"owner","type":"address"}],"name":"balanceOf","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"baseTokenURI","outputs":[{"internalType":"string","name":"","type":"string"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"tokenId","type":"uint256"}],"name":"getApproved","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"owner","type":"address"},{"internalType":"address","name":"operator","type":"address"}],"name":"isApprovedForAll","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"recipient","type":"address"},{"internalType":"string","name":"uri","type":"string"}],"name":"mint","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"name","outputs":[{"internalType":"string","name":"","type":"string"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"tokenId","type":"uint256"}],"name":"ownerOf","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"from","type":"address"},{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"tokenId","type":"uint256"}],"name":"safeTransferFrom","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"from","type":"address"},{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"tokenId","type":"uint256"},{"internalType":"bytes","name":"_data","type":"bytes"}],"name":"safeTransferFrom","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"operator","type":"address"},{"internalType":"bool","name":"approved","type":"bool"}],"name":"setApprovalForAll","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"bytes4","name":"interfaceId","type":"bytes4"}],"name":"supportsInterface","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"symbol","outputs":[{"internalType":"string","name":"","type":"string"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"tokenId","type":"uint256"}],"name":"tokenURI","outputs":[{"internalType":"string","name":"","type":"string"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"from","type":"address"},{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"tokenId","type":"uint256"}],"name":"transferFrom","outputs":[],"stateMutability":"nonpayable","type":"function"}]`;
 const EXCHANGE_CONTRACT_ABI = `[{"inputs":[{"internalType":"string","name":"_contractName","type":"string"},{"internalType":"string","name":"_contractVersion","type":"string"},{"internalType":"address","name":"_admin","type":"address"}],"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"uint256","name":"exchangeId","type":"uint256"}],"name":"Exchange","type":"event"},{"anonymous":false,"inputs":[],"name":"Paused","type":"event"},{"anonymous":false,"inputs":[],"name":"Unpaused","type":"event"},{"inputs":[{"internalType":"address","name":"_account","type":"address"}],"name":"addAdmin","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"adminCount","outputs":[{"internalType":"uint16","name":"","type":"uint16"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"","type":"address"}],"name":"admins","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"view","type":"function"},{"inputs":[{"components":[{"internalType":"string","name":"_nonce","type":"string"},{"internalType":"uint256","name":"_startsAt","type":"uint256"},{"internalType":"uint256","name":"_expiresAt","type":"uint256"},{"internalType":"address","name":"_nftContract","type":"address"},{"internalType":"uint256","name":"_nftTokenId","type":"uint256"},{"internalType":"address","name":"_paymentTokenContract","type":"address"},{"internalType":"address","name":"_seller","type":"address"},{"internalType":"address","name":"_royaltyPayTo","type":"address"},{"internalType":"uint256","name":"_sellerAmount","type":"uint256"},{"internalType":"uint256","name":"_feeAmount","type":"uint256"},{"internalType":"uint256","name":"_royaltyAmount","type":"uint256"},{"internalType":"uint256","name":"_totalAmount","type":"uint256"}],"internalType":"struct NFTexchange.SellOrder","name":"sell","type":"tuple"},{"internalType":"uint256","name":"exchangeId","type":"uint256"},{"internalType":"bytes","name":"_signature","type":"bytes"}],"name":"buyNFT","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"payable","type":"function"},{"inputs":[],"name":"chainId","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"_account","type":"address"}],"name":"deleteAdmin","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"components":[{"internalType":"string","name":"_nonce","type":"string"},{"internalType":"uint256","name":"_startsAt","type":"uint256"},{"internalType":"uint256","name":"_expiresAt","type":"uint256"},{"internalType":"address","name":"_nftContract","type":"address"},{"internalType":"uint256","name":"_nftTokenId","type":"uint256"},{"internalType":"address","name":"_paymentTokenContract","type":"address"},{"internalType":"address","name":"_seller","type":"address"},{"internalType":"address","name":"_royaltyPayTo","type":"address"},{"internalType":"uint256","name":"_sellerAmount","type":"uint256"},{"internalType":"uint256","name":"_feeAmount","type":"uint256"},{"internalType":"uint256","name":"_royaltyAmount","type":"uint256"},{"internalType":"uint256","name":"_totalAmount","type":"uint256"}],"internalType":"struct NFTexchange.SellOrder","name":"sell","type":"tuple"},{"components":[{"internalType":"string","name":"_nonce","type":"string"},{"internalType":"uint256","name":"_startsAt","type":"uint256"},{"internalType":"uint256","name":"_expiresAt","type":"uint256"},{"internalType":"address","name":"_nftContract","type":"address"},{"internalType":"uint256","name":"_nftTokenId","type":"uint256"},{"internalType":"address","name":"_paymentTokenContract","type":"address"},{"internalType":"address","name":"_buyer","type":"address"},{"internalType":"address","name":"_royaltyPayTo","type":"address"},{"internalType":"uint256","name":"_sellerAmount","type":"uint256"},{"internalType":"uint256","name":"_feeAmount","type":"uint256"},{"internalType":"uint256","name":"_royaltyAmount","type":"uint256"},{"internalType":"uint256","name":"_totalAmount","type":"uint256"}],"internalType":"struct NFTexchange.BuyOrder","name":"buy","type":"tuple"},{"internalType":"uint256","name":"exchangeId","type":"uint256"},{"internalType":"uint256","name":"minBidAmountToExecute","type":"uint256"},{"internalType":"bytes","name":"_sellerSig","type":"bytes"},{"internalType":"bytes","name":"_buyerSig","type":"bytes"}],"name":"exchangeNFTauction","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"getAllAdmins","outputs":[{"internalType":"address[]","name":"","type":"address[]"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"name","outputs":[{"internalType":"string","name":"","type":"string"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"pauseContract","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"paused","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"view","type":"function"},{"inputs":[{"components":[{"internalType":"string","name":"_nonce","type":"string"},{"internalType":"uint256","name":"_startsAt","type":"uint256"},{"internalType":"uint256","name":"_expiresAt","type":"uint256"},{"internalType":"address","name":"_nftContract","type":"address"},{"internalType":"uint256","name":"_nftTokenId","type":"uint256"},{"internalType":"address","name":"_paymentTokenContract","type":"address"},{"internalType":"address","name":"_buyer","type":"address"},{"internalType":"address","name":"_royaltyPayTo","type":"address"},{"internalType":"uint256","name":"_sellerAmount","type":"uint256"},{"internalType":"uint256","name":"_feeAmount","type":"uint256"},{"internalType":"uint256","name":"_royaltyAmount","type":"uint256"},{"internalType":"uint256","name":"_totalAmount","type":"uint256"}],"internalType":"struct NFTexchange.BuyOrder","name":"buy","type":"tuple"},{"internalType":"uint256","name":"exchangeId","type":"uint256"},{"internalType":"bytes","name":"_signature","type":"bytes"}],"name":"sellNFT","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"unPauseContract","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"version","outputs":[{"internalType":"string","name":"","type":"string"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"_tokenContract","type":"address"},{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"amount","type":"uint256"}],"name":"withdrawERC20Token","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address payable","name":"to","type":"address"},{"internalType":"uint256","name":"amountInWei","type":"uint256"}],"name":"withdrawETH","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"nonpayable","type":"function"}]`;


const web3 = require('@alch/alchemy-web3').createAlchemyWeb3(
  // 'https://eth-ropsten.alchemyapi.io/v2/dY6IrLYtOxpPqVh0LzRK35QoT9Esm0z6',
  // 'https://eth-rinkeby.alchemyapi.io/v2/dY6IrLYtOxpPqVh0LzRK35QoT9Esm0z6',
  // 'wss://eth-rinkeby.alchemyapi.io/v2/dY6IrLYtOxpPqVh0LzRK35QoT9Esm0z6'
  // 'https://rinkeby.infura.io/v3/c5c5fb51e09647a684bea51e5f9668c7',
  // 'wss://rinkeby.infura.io/ws/v3/c5c5fb51e09647a684bea51e5f9668c7',
  // 'https://bsc.getblock.io/testnet/?api_key=0d438d8c-a6a3-4477-9da2-3824b55de02f',
  'wss://bsc.getblock.io/testnet/?api_key=0d438d8c-a6a3-4477-9da2-3824b55de02f',
);

// let web3 = require('web3');
// web3 = new web3(
//   'https://rinkeby.infura.io/v3/065fecc7aaff497cbdb1eac4ba4b30b8',
// );

const nftAddress = '0x8F902E2dc9863e5345818892f99B6D7334c698DB';
//'0x015EEE8C0Ff2f7f9738Fc579347658bbb7D8Dd6F';

const excAddress = '0xE4BE65c08418CeC40D6A4B313d5593ebFD50EdF0';
//rinkey '0x015EEE8C0Ff2f7f9738Fc579347658bbb7D8Dd6F';
//ropsten '0x0228A456B4719Dd584230202b9FF47c986Ad7893';

const privateKey =
  '673d6685548a71cfcc282b5d44f7f0e3256be1580cc915aea2ffe8b00b5bc3cf';
const account = web3.eth.accounts.privateKeyToAccount(privateKey);
web3.eth.defaultAccount = account.address;

const nftContract = new web3.eth.Contract(JSON.parse(ERC721_ABI), nftAddress);
const excContract = new web3.eth.Contract(JSON.parse(EXCHANGE_CONTRACT_ABI), excAddress);
// console.log(nftContract);

async function getAllNfts() {
  const nfts = await web3.alchemy.getNfts({
    owner: '0x31d4Ae75Cd68f3eaEfAa8b77fF1bc505FBC32184',
    contractAddresses: ['0x015EEE8C0Ff2f7f9738Fc579347658bbb7D8Dd6F'],
  });
  console.log(nfts);
}

//getAllNfts();

async function transfer() {
  const call = nftContract.methods.safeTransferFrom(
    '0x9908CbCb070d1ed8d8f2c064b281D3029545b185',
    '0x31d4Ae75Cd68f3eaEfAa8b77fF1bc505FBC32184',
    1,
  );
  const tx = {
    // from: '0x9908CbCb070d1ed8d8f2c064b281D3029545b185',
    to: nftAddress,
    gas: 2000000,
    gasPrice: 10,
    data: call.encodeABI(),
  };
  const signedTx = await web3.eth.accounts.signTransaction(tx, privateKey);
  const receipt = await web3.eth.sendSignedTransaction(signedTx.rawTransaction);
  console.log(receipt);
}
// transfer();

async function setApprovalForAll() {
  const call = nftContract.methods.setApprovalForAll(
    '0xb3B738b13b02890483C3D1010f42c2B511BC9a7F',
    true,
  );
  const estimatedGas = await call.estimateGas();
  console.log(estimatedGas);
  const tx = {
    to: nftAddress,
    gas: 2000000,
    data: call.encodeABI(),
  };
  const signedTx = await web3.eth.accounts.signTransaction(tx, privateKey);
  const receipt = await web3.eth.sendSignedTransaction(signedTx.rawTransaction);
  // console.log(receipt);
  // await getConfirmedTransaction(receipt.transactionHash);
  // console.log(txObj);
}
// setApprovalForAll();

// 0x2c2a29e6ac84b4fb0015723a10cf67b9d2314e5935b0e7182ea052f182e1ce18
async function getConfirmedTransaction(
  txHash = '0x4e7e06bded6f2e537f2bf4c9bcf0c8ce6e8ccec706517612353baf80faf684ca',
) {
  const tx = await web3.eth.getTransactionReceipt(txHash);
  console.log(web3.utils.hexToNumberString(tx.logs[0].topics[3]));
  const currentBlock = await web3.eth.getBlockNumber();
  console.log(currentBlock);
  console.log(currentBlock - tx.blockNumber);
}
// getConfirmedTransaction();

async function ownerOf() {
  /* const call = nftContract.methods.setApprovalForAll(
    '0xb3B738b13b02890483C3D1010f42c2B511BC9a7F',
    true,
  );
  const estimatedGas = await call.estimateGas();
  console.log(estimatedGas); */
  const tx = await nftContract.methods.ownerOf(1).call();
  console.log(tx);
}

// ownerOf();

async function admins() {
  const result = await excContract.methods.getAllAdmins().call();
  console.log(result);
}
// admins();

function testError() {
  console.log(new Date('sdfsdfsdfsdf'));
}
// testError();

async function Event() {
  nftContract.events.ApprovalForAll({
    //filter: {myIndexedParam: [20,23], myOtherIndexedParam: '0x123456789...'}, // Using an array means OR: e.g. 20 or 23
    //fromBlock: 0
  } /*, function(error, event){ console.log(event); } */)
    .on("connected", function (subscriptionId) {
      console.log(subscriptionId);
    })
    .on('data', function (event) {
      console.log('on data');
      console.log(event); // same results as the optional callback above
    })
    .on('changed', function (event) {
      console.log('on changed');
      console.log(event);
      // remove event from local database

    })
    .on('error', function (error, receipt) { // If the transaction was rejected by the network with a receipt, the second parameter will be the receipt.
      console.log('on error');
      console.log(error);
      console.log(receipt);
    });
}
Event();

const InputDataDecoder = require('ethereum-input-data-decoder');
const abi = [{ "inputs": [{ "internalType": "string", "name": "_contractName", "type": "string" }, { "internalType": "string", "name": "_contractVersion", "type": "string" }, { "internalType": "address", "name": "_admin", "type": "address" }], "stateMutability": "nonpayable", "type": "constructor" }, { "anonymous": false, "inputs": [{ "indexed": false, "internalType": "string", "name": "exchangeId", "type": "string" }], "name": "Exchange", "type": "event" }, { "anonymous": false, "inputs": [], "name": "Paused", "type": "event" }, { "anonymous": false, "inputs": [], "name": "Unpaused", "type": "event" }, { "inputs": [{ "internalType": "address", "name": "_account", "type": "address" }], "name": "addAdmin", "outputs": [{ "internalType": "bool", "name": "", "type": "bool" }], "stateMutability": "nonpayable", "type": "function" }, { "inputs": [], "name": "adminCount", "outputs": [{ "internalType": "uint16", "name": "", "type": "uint16" }], "stateMutability": "view", "type": "function" }, { "inputs": [{ "internalType": "address", "name": "", "type": "address" }], "name": "admins", "outputs": [{ "internalType": "bool", "name": "", "type": "bool" }], "stateMutability": "view", "type": "function" }, { "inputs": [{ "components": [{ "internalType": "string", "name": "_nonce", "type": "string" }, { "internalType": "uint256", "name": "_startsAt", "type": "uint256" }, { "internalType": "uint256", "name": "_expiresAt", "type": "uint256" }, { "internalType": "address", "name": "_nftContract", "type": "address" }, { "internalType": "uint256", "name": "_nftTokenId", "type": "uint256" }, { "internalType": "address", "name": "_paymentTokenContract", "type": "address" }, { "internalType": "address", "name": "_seller", "type": "address" }, { "internalType": "uint256", "name": "_sellerAmount", "type": "uint256" }, { "internalType": "uint256", "name": "_feeWithRoyalty", "type": "uint256" }, { "internalType": "uint256", "name": "_totalAmount", "type": "uint256" }], "internalType": "struct NFTexchange.SellOrder", "name": "sell", "type": "tuple" }, { "internalType": "string", "name": "exchangeId", "type": "string" }, { "internalType": "bytes", "name": "_signature", "type": "bytes" }], "name": "buyNFT", "outputs": [{ "internalType": "bool", "name": "", "type": "bool" }], "stateMutability": "payable", "type": "function" }, { "inputs": [], "name": "chainId", "outputs": [{ "internalType": "uint256", "name": "", "type": "uint256" }], "stateMutability": "view", "type": "function" }, { "inputs": [{ "internalType": "address", "name": "_account", "type": "address" }], "name": "deleteAdmin", "outputs": [{ "internalType": "bool", "name": "", "type": "bool" }], "stateMutability": "nonpayable", "type": "function" }, { "inputs": [{ "components": [{ "internalType": "string", "name": "_nonce", "type": "string" }, { "internalType": "uint256", "name": "_startsAt", "type": "uint256" }, { "internalType": "uint256", "name": "_expiresAt", "type": "uint256" }, { "internalType": "address", "name": "_nftContract", "type": "address" }, { "internalType": "uint256", "name": "_nftTokenId", "type": "uint256" }, { "internalType": "address", "name": "_paymentTokenContract", "type": "address" }, { "internalType": "address", "name": "_seller", "type": "address" }, { "internalType": "uint256", "name": "_sellerAmount", "type": "uint256" }, { "internalType": "uint256", "name": "_feeWithRoyalty", "type": "uint256" }, { "internalType": "uint256", "name": "_totalAmount", "type": "uint256" }], "internalType": "struct NFTexchange.SellOrder", "name": "sell", "type": "tuple" }, { "components": [{ "internalType": "string", "name": "_nonce", "type": "string" }, { "internalType": "uint256", "name": "_startsAt", "type": "uint256" }, { "internalType": "uint256", "name": "_expiresAt", "type": "uint256" }, { "internalType": "address", "name": "_nftContract", "type": "address" }, { "internalType": "uint256", "name": "_nftTokenId", "type": "uint256" }, { "internalType": "address", "name": "_paymentTokenContract", "type": "address" }, { "internalType": "address", "name": "_buyer", "type": "address" }, { "internalType": "uint256", "name": "_sellerAmount", "type": "uint256" }, { "internalType": "uint256", "name": "_feeWithRoyalty", "type": "uint256" }, { "internalType": "uint256", "name": "_totalAmount", "type": "uint256" }], "internalType": "struct NFTexchange.BuyOrder", "name": "buy", "type": "tuple" }, { "internalType": "string", "name": "exchangeId", "type": "string" }, { "internalType": "uint256", "name": "minBidAmountToExecute", "type": "uint256" }, { "internalType": "bytes", "name": "_sellerSig", "type": "bytes" }, { "internalType": "bytes", "name": "_buyerSig", "type": "bytes" }], "name": "exchangeNFTauction", "outputs": [{ "internalType": "bool", "name": "", "type": "bool" }], "stateMutability": "nonpayable", "type": "function" }, { "inputs": [], "name": "getAllAdmins", "outputs": [{ "internalType": "address[]", "name": "", "type": "address[]" }], "stateMutability": "view", "type": "function" }, { "inputs": [], "name": "name", "outputs": [{ "internalType": "string", "name": "", "type": "string" }], "stateMutability": "view", "type": "function" }, { "inputs": [], "name": "pauseContract", "outputs": [{ "internalType": "bool", "name": "", "type": "bool" }], "stateMutability": "nonpayable", "type": "function" }, { "inputs": [], "name": "paused", "outputs": [{ "internalType": "bool", "name": "", "type": "bool" }], "stateMutability": "view", "type": "function" }, { "inputs": [{ "components": [{ "internalType": "string", "name": "_nonce", "type": "string" }, { "internalType": "uint256", "name": "_startsAt", "type": "uint256" }, { "internalType": "uint256", "name": "_expiresAt", "type": "uint256" }, { "internalType": "address", "name": "_nftContract", "type": "address" }, { "internalType": "uint256", "name": "_nftTokenId", "type": "uint256" }, { "internalType": "address", "name": "_paymentTokenContract", "type": "address" }, { "internalType": "address", "name": "_buyer", "type": "address" }, { "internalType": "uint256", "name": "_sellerAmount", "type": "uint256" }, { "internalType": "uint256", "name": "_feeWithRoyalty", "type": "uint256" }, { "internalType": "uint256", "name": "_totalAmount", "type": "uint256" }], "internalType": "struct NFTexchange.BuyOrder", "name": "buy", "type": "tuple" }, { "internalType": "string", "name": "exchangeId", "type": "string" }, { "internalType": "bytes", "name": "_signature", "type": "bytes" }], "name": "sellNFT", "outputs": [{ "internalType": "bool", "name": "", "type": "bool" }], "stateMutability": "nonpayable", "type": "function" }, { "inputs": [], "name": "unPauseContract", "outputs": [{ "internalType": "bool", "name": "", "type": "bool" }], "stateMutability": "nonpayable", "type": "function" }, { "inputs": [], "name": "version", "outputs": [{ "internalType": "string", "name": "", "type": "string" }], "stateMutability": "view", "type": "function" }, { "inputs": [{ "internalType": "address", "name": "_tokenContract", "type": "address" }, { "internalType": "address", "name": "to", "type": "address" }, { "internalType": "uint256", "name": "amount", "type": "uint256" }], "name": "withdrawERC20Token", "outputs": [{ "internalType": "bool", "name": "", "type": "bool" }], "stateMutability": "nonpayable", "type": "function" }, { "inputs": [{ "internalType": "address payable", "name": "to", "type": "address" }, { "internalType": "uint256", "name": "amountInWei", "type": "uint256" }], "name": "withdrawETH", "outputs": [{ "internalType": "bool", "name": "", "type": "bool" }], "stateMutability": "nonpayable", "type": "function" }];
const decoder = new InputDataDecoder(abi);
const data = '0xab6af58900000000000000000000000000000000000000000000000000000000000000c0000000000000000000000000000000000000000000000000000000000000024000000000000000000000000000000000000000000000000000000000000003c0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000004000000000000000000000000000000000000000000000000000000000000000480000000000000000000000000000000000000000000000000000000000000014000000000000000000000000000000000000000000000000000000000626aaf4b00000000000000000000000000000000000000000000000000000000626ab2f8000000000000000000000000015eee8c0ff2f7f9738fc579347658bbb7d8dd6f000000000000000000000000000000000000000000000000000000000000002e000000000000000000000000c778417e063141139fce010982780140aa0cd5ab000000000000000000000000c45f998e5a39b08ab2fefad61de450de0b2dc95700000000000000000000000000000000000000000000000000035b78f550100000000000000000000000000000000000000000000000000000003205af766fff00000000000000000000000000000000000000000000000000038d7ea4c6800000000000000000000000000000000000000000000000000000000000000000063339383238340000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000014000000000000000000000000000000000000000000000000000000000626ab03f00000000000000000000000000000000000000000000000000000000626c0478000000000000000000000000015eee8c0ff2f7f9738fc579347658bbb7d8dd6f000000000000000000000000000000000000000000000000000000000000002e000000000000000000000000c778417e063141139fce010982780140aa0cd5ab0000000000000000000000003f64e4b25862bde8ba388a9ad71d336b4d0f4332000000000000000000000000000000000000000000000000000a126adff03000000000000000000000000000000000000000000000000000000096110e635000000000000000000000000000000000000000000000000000000aa87bee53800000000000000000000000000000000000000000000000000000000000000000063031373134340000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000002034333365623162346666636134643262383031336435326631626166316235330000000000000000000000000000000000000000000000000000000000000041565cea38a762456e730e1f815c25d7b785d53848bc7e428cb56893d7e3654f7f1e489c3ea6a6ac5cc6b318ec81ab230575c85d3935d2ad31105bbf9e7c4156cd1b000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000041397e8b6536d0b2bbde134337273bc45e47b9aa0752520f61bd79a6e3d8be71c03cff84d88feac8e6c590ddb9c29b76300bdc26ba36a0c311a4c9dcea8ce4a2cd1b00000000000000000000000000000000000000000000000000000000000000';
const result = decoder.decodeData(data);
// console.log(JSON.stringify(result));

function testDecode() {
  console.log(web3.eth.abi.decodeParameter('address', '0x0000000000000000000000003f64e4b25862bde8ba388a9ad71d336b4d0f4332'));
}
// testDecode();

async function test() {
  const count = await web3.eth.getTransactionCount('0x9908CbCb070d1ed8d8f2c064b281D3029545b185', 'latest');
  console.log(count);
}
// test();