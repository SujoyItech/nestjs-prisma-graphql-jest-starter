import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { setApp } from '../src/app/helpers/functions';
import { MainModule } from '../src/main.module';
import * as path from 'path';
import * as fs from 'fs';
import { graphqlUploadExpress } from 'graphql-upload';
import { getApplication } from './utils/get-application';

describe('Category Resolver (e2e) test', () => {
  let app: INestApplication;
  let BEARER_TOKEN;

  beforeAll(async () => {
    app = await getApplication();
    app.use(graphqlUploadExpress());
    // await app.init();
    // const mutation = () => `
    //       mutation StaffLogin($username: String!, $password: String!){
    //         StaffLogin(data: {username : $username, password: $password}){
    //             accessToken
    //             refreshToken
    //             expireAt
    //         }
    //     }`;

    // const { body } = await request(app.getHttpServer())
    //   .post('/graphql')
    //   .send({
    //     query: mutation(),
    //     variables: {
    //       username: 'admin@email.com',
    //       password: '12345678',
    //     },
    //   });
    // BEARER_TOKEN = body.data.StaffLogin.accessToken;
  });

  // describe('getCategories', () => {
  //   it('Should returns an category', async () => {
  //     const query = () => `
  //       query getCategories{
  //         getCategories{
  //           id
  //           title
  //           image
  //           status
  //         }
  //       }`;

  //     const { body } = await request(app.getHttpServer())
  //       .post('/graphql')
  //       .send({
  //         query: query(),
  //         variables: {},
  //       });

  //     expect(body).toHaveProperty('data');
  //     expect(body.data).toHaveProperty('getCategories');
  //   });
  // });
  // describe('createCategory', () => {
  //   it('Should returns an category', async () => {
  //     try {
  //       const mutation = `
  //       mutation createCategory($title: String!, $imageFile: Upload, $status: Int) {
  //         createCategory(data: { title: $title, imageFile: $imageFile, status: $status }) {
  //           success
  //           message
  //         }
  //       }`;
  //       // const imagePath = './images/binance.png';
  //       // const fixturePath = path.join(__dirname, imagePath);
  //       // console.log(fixturePath);
  //       // const fileStream = fs.createReadStream(imagePath);
  //       const { body } = await request(app.getHttpServer())
  //         .post('/graphql')
  //         .set('Authorization', 'Bearer ' + BEARER_TOKEN)
  //         // .set('Content-Type', 'multipart/form-data')
  //         .send({
  //           query: mutation,
  //           variables: {
  //             title: 'Neq test',
  //           },
  //         });
  //       console.log(body);
  //       expect(body).toHaveProperty('data');
  //       expect(body.data).toHaveProperty('createCategory');
  //     } catch (error) {
  //       console.log(error);
  //     }
  //   });
  // });
  describe('createCategoryImage', () => {
    it('Should returns an category', async () => {
      //try {
      const mutation = `
        mutation createCategory($title: String!, $imageFile: Upload, $status: Int) {
          createCategory(data: { title: $title, imageFile: $imageFile, status: $status }) {
            success
            message
          }
        }`;
      const imagePath = './images/test.png';
      const fixturePath = path.join(__dirname, imagePath);
      console.log(fixturePath);
      // const fileStream = fs.createReadStream(imagePath);
      const { body } = await request(process.env.APP_URL)
        .post('/graphql')
        .field(
          'operations',
          JSON.stringify({
            operationName: null,
            variables: {
              title: 'Demo test 234242343 rrrr',
              imageFile: null,
            },
            query: mutation,
          }),
        )
        .field(
          'map',
          JSON.stringify({
            '0': ['variables.imageFile'],
          }),
        )
        .attach('0', fixturePath)
        // .set('Authorization', 'Bearer ' + BEARER_TOKEN)
        // .set('Content-Type', 'multipart/form-data')
        // .set('Accept', 'application/json')
        // .field(
        //   'operations',
        //   JSON.stringify({
        //     query: mutation,
        //     variables: {
        //       title: 'Neq test',
        //     },
        //   }),
        // )
        // .field('map', `0: ['variables.imageFile']`)
        // .attach('0', fixturePath)
        .expect(200);
      expect(body).toHaveProperty('data');
      expect(body.data).toHaveProperty('createCategory');
      // } catch (error) {
      //   console.log(error);
      // }
    });
  });
  // afterAll(async () => {
  //   await app.close();
  // });
});
