import { Test } from '@nestjs/testing';
import { MainModule } from '../../src/main.module';

export async function createTestingModule() {
  const moduleBuilder = Test.createTestingModule({
    imports: [MainModule],
  });

  const compiled = await moduleBuilder.compile();

  const app = compiled.createNestApplication(undefined, {
    logger: false,
  });

  return await app.init();
}
