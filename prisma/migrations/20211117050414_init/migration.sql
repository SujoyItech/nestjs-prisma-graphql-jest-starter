/*
  Warnings:

  - You are about to drop the column `role` on the `staffs` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "staffs" DROP COLUMN "role",
ADD COLUMN     "roleId" INTEGER;

-- AddForeignKey
ALTER TABLE "staffs" ADD CONSTRAINT "staffs_roleId_fkey" FOREIGN KEY ("roleId") REFERENCES "roles"("id") ON DELETE SET NULL ON UPDATE CASCADE;
