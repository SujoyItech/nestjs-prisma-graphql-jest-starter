-- CreateTable
CREATE TABLE "templates" (
    "id" SERIAL NOT NULL,
    "name" TEXT NOT NULL DEFAULT E'default',
    "title" TEXT NOT NULL DEFAULT E'Default Template',
    "channel" TEXT NOT NULL DEFAULT E'email',
    "params" TEXT,
    "content" TEXT,

    CONSTRAINT "templates_pkey" PRIMARY KEY ("id")
);
