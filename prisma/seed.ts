import { PrismaClient } from '@prisma/client';
import { seedStaffs } from './seeds/staff.seeder';
import { seedCategories } from '../prisma/seeds/category.seeder';
const prisma = new PrismaClient({ log: ['query'] });

async function main() {
  await seedStaffs(prisma);
  await seedCategories(prisma);
}

main()
  .catch((e) => {
    console.error(e.stack);
    process.exit(1);
  })
  .finally(async () => {
    await prisma.$disconnect();
  });
